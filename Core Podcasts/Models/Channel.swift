//
//  Channel.swift
//  Core Podcasts
//
//  Created by BrianBolze on 5/31/19.
//  Copyright © 2019 Core Wellness. All rights reserved.
//

import Foundation

struct Channel {
  let title: String
  let description: String
  let createdDate: Date
  let topics: [Topic]
  let episodes: [Episode]
  
  let mostPopularEpisode: [Episode]
  let numberOfPlaysTotal: Int32
  let numberOfSubscribers: Int32
}
