//
//  Episode.swift
//  Core Podcasts
//
//  Created by BrianBolze on 5/31/19.
//  Copyright © 2019 Core Wellness. All rights reserved.
//

import Foundation

struct Episode {
  let title: String
  let description: String
}
