//
//  UserAccount.swift
//  Core Podcasts
//
//  Created by BrianBolze on 5/31/19.
//  Copyright © 2019 Core Wellness. All rights reserved.
//

import Foundation

struct UserAccount {
  let firstName: String
  let lastName: String
  let email: String
  let favoriteTopics: [Topic]
  let subscribedChannels: [Channel]
}
