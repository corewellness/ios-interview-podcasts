//
//  Topic.swift
//  Core Podcasts
//
//  Created by BrianBolze on 5/31/19.
//  Copyright © 2019 Core Wellness. All rights reserved.
//

import Foundation

struct Topic {
  let name: String
  let description: String
}
